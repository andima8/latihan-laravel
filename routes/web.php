<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return view('datatables');
});

//show data array
Route::get('/pertanyaan', 'PertanyaanController@index');


//Create new data
Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::post('/pertanyaan', 'PertanyaanController@store');

//show data detail
Route::get('/pertanyaan/{id}', 'PertanyaanController@show');

//edit data
Route::get('/pertanyaan/{id}/edit', 'PertanyaanController@edit');

//update data
Route::put('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@update');

//delete data
Route::delete('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@destroy');

Route::get('/register', 'AuthController@register');
Route::post('/register/welcome', 'AuthController@getName');