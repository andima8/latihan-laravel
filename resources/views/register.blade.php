<!DOCTYPE html>
<html>
    <head>
    <title>Sign Up</title>
    </head>

    <body>
        <div>
            <h1>Buat Account Baru!</h1>
            <h3>Sign Up Form</h3>
        </div>

        <div>
            <form method="POST" action="/register/welcome">
               @csrf
                <div>
                    <label for="first_name">First Name:</label>
                    <br>
                    <br>
                    <input type="text" name="first_name" id="first_name">
                    <br>
                    <label for="last_name">Last Name:</label>
                    <br>
                    <br>
                    <input type="text" name="last_name" id="last_name">
                    <br>
                    <br>
                </div>

                <div>
                    <label>Gender:</label>
                    <br>
                    <br>
                    <input type="radio" name="gender" value="0">Male 
                    <br>
                    <input type="radio" name="gender" value="1">Female
                    <br>
                    <br>
                </div>
                <div>
                    <label>Nationality:</label>
                    <br>
                    <br>
                    <select>
                        <option name="Nationality" value="{{session()->get('user1')}}" >Indonesian</option>
                        <option name="Nationality">Malaysian</option>
                        <option name="Nationality">Singaporean</option>
                        <option name="Nationality">Australian</option>
                    </select>
                    <br>
                    <br>
                </div>
                <div>
                    <label>Language Spoken:</label>
                    <br>
                    <br>
                    <input type="checkbox" name="spoken" value="0">Bahasa Indonesia
                    <br>
                    <input type="checkbox" name="spoken" value="1">English
                    <br>
                    <input type="checkbox" name="spoken" value="2">Arabic
                    <br>
                    <input type="checkbox" name="spoken" value="3">Japanese
                    <br>
                    <br>
                </div>
                <div>
                    <label for="bio_user">Bio:</label>
                    <br>
                    <Textarea id="bio_user" cols="30" rows="10"></Textarea>
                    <br>
                </div>
                <input type="submit" value="Sign Up"">
            </form>
        </div>
    </body>    

</html>