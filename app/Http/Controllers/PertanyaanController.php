<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create(){
        return view('posts.create');
    }

    //get Detail
    public function show($id) {
        $post = DB::table('pertanyaan')->where('id', $id)->first();
        return view('show.detail', compact('post'));
    }

    //update Data
    public function update($id, Request $request){

        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required',
        ]);

        $query = DB::table('pertanyaan')
                    ->where('id', $id)
                    ->update([
                        'judul' => $request['judul'],
                        'isi' => $request['isi']
                    ]);

        return redirect('/pertanyaan')->with('success', 'Data berhasil diperbarui');
    }

    //edit Data
    public function edit($id){
        $post = DB::table('pertanyaan')->where('id', $id)->first();
        return view('show.edit', compact('post'));
    }

    public function destroy($id){
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil dihapus');
    }

    //post Data
    public function store(Request $request) {
        //unique.namaTable
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required',
        ]);

       $query = DB::table('pertanyaan')->insert([
           "judul" => $request["judul"],
           "isi" => $request["isi"]
           ]);

           return redirect('/pertanyaan')->with('success','Pertanyaan berhasil disimpan');

    }

    //get Data in Array
    function index() {
        $show = DB::table('pertanyaan')->get(); //select * from pertanyaan
        //dd($show);
        return view('show.show', compact('show'));
    }

    
}
