<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function getName(Request $request){
       //dd($request->all());
       $first = $request["first_name"];
       $last = $request["last_name"];
       return view('welcome', compact('first', 'last'));
    }

}
